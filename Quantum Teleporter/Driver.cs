﻿using Microsoft.Quantum.Simulation.Simulators;
using System;

namespace Quantum.Quantum_Teleporter
{
    class Driver
    {
        static void Main(string[] args)
        {
            using (var experiment = new QuantumSimulator())
            {
                var teleportationResults = Operation.Run(experiment).Result;
                Console.WriteLine($"Teleportation results after 100 runs: {teleportationResults}");
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}