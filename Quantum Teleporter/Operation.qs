﻿namespace Quantum.Quantum_Teleporter
{
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;

	operation Set (toSet: Qubit, expected: Result) : ()
	{
		body
		{
			if(M(toSet) != expected)
			{
				X(toSet);
			}
		}
	}

    operation Operation () : Int
    {
        body
        {
			mutable measuredOne = 0;
			using(registers = Qubit[3])
			{
				for(counter in 1..100)
				{
					// create state to transport
					let bones = registers[2];
					H(bones);
					S(bones);
					T(bones);

					// prepare transport
					let transportBeam = registers[0];
					let transporter = registers[1];
					H(transporter);
					CNOT(transporter, transportBeam);
					CNOT(bones, transporter);
					H(bones);
					
					// measure needed adjustments
					mutable needToFlip = false;
					if(M(transporter) == One) { set needToFlip = true; } 

					mutable needToPhaseShift = false;
					if(M(bones) == One) { set needToPhaseShift = true; }



					// send qubit and data off to receiver

					// Fieser Spion
					if(M(transportBeam) == One)
					{
						// evil stuff
					}


					// receive and adjust
					if(needToFlip)
					{
						X(transportBeam);
					}

					if(needToPhaseShift)
					{
						Z(transportBeam);
					}
					
					// receiver measures the qubit
					H(transportBeam);
					let measurement = M(transportBeam);
					if(measurement == One)
					{
						set measuredOne = measuredOne + 1;
					}
					
					Set(transporter, Zero);
					Set(transportBeam, Zero);
					Set(bones, Zero);
				}
			}
			return measuredOne;
        }
    }
}
